import { AppRoutes, Footer, Header } from './components';

function App() {
  return (
    <div className="dark:bg-gray-800">
      <Header></Header>
      <AppRoutes></AppRoutes>
      <Footer></Footer>
    </div>
  );
}

export default App;
