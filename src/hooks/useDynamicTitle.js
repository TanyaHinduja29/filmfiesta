import { useEffect } from 'react'

export const useDynamicTitle = (title, defaultTitle="File Fiesta") => {
  useEffect(() => {
    document.title = title;

    return () =>{
        document.title = defaultTitle;
    }
  });
}
