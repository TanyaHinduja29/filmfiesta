import React, { useEffect } from "react";
import { MovieCard } from "../components";
import useFetch from "../hooks/useFetch";
import { MovieCardSkeleton } from "../components/MovieCardSkeleton";
import { useDynamicTitle } from "../hooks/useDynamicTitle";

export const MoviePage = ({ apiPath, title }) => {
  const BASE_API = process.env.REACT_APP_API_URL;
  const {data: movies, isLoading, setUrl} = useFetch();

  useEffect(() =>{
    setUrl(`${BASE_API}${apiPath}?api_key=${process.env.REACT_APP_API_KEY}`);
  },[apiPath, BASE_API, setUrl])

  useDynamicTitle(title);
  function renderSkeletons(count){
    const skeletons = [];
    for(let i=1; i<=count;i++){
      skeletons.push(<MovieCardSkeleton key={i}/>)
    }
    return skeletons;
  }
  return (
    <main>
      <div className="flex flex-wrap justify-start">
        {
          isLoading && renderSkeletons(6)
        }
        {
         !isLoading && movies && movies.results.map(movie => <MovieCard movie={movie} key={movie.id}></MovieCard>)
        }
      </div>
    </main>
  );
};
