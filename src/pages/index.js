export {MovieDetailsPage} from './movieDetails/MovieDetailsPage'
export {MoviePage} from './MoviePage'
export {PageNotFound} from './PageNotFound'
export {SearchPage} from './SearchPage'
export {DetailMovie} from './movieDetails/DetailMovie';
export {DetailSkeleton} from './movieDetails/DetailSkeleton';