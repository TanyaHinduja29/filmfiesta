import React, { useEffect } from 'react'
import { useParams } from 'react-router-dom';
import { useFetch } from '../../hooks';
import { DetailMovie } from './DetailMovie';
import { DetailSkeleton } from './DetailSkeleton';

export const MovieDetailsPage = () => {
  const params = useParams();
  const {data: movie, setUrl, isLoading} = useFetch();

  useEffect(() => {
    const movieId = params.id;
    const URL = `${process.env.REACT_APP_API_URL}movie/${movieId}?api_key=${process.env.REACT_APP_API_KEY}`;
    setUrl(URL);
  });
  return (
    <main>
        { isLoading && <DetailSkeleton></DetailSkeleton>}
        { movie && <DetailMovie movie={movie}></DetailMovie>}
    </main>
    
  )
}
